import { Router } from "express";
import { PermissionController } from "../controllers/permission.controller";
import { PermissionRepository } from "../repositories/permission.repository";
import { PermissionTypeRepository } from "../repositories/permissionType.repository";

const router = Router();
const permissionsController = new PermissionController(new PermissionRepository(), new PermissionTypeRepository());

router.post("/permissions",permissionsController.create.bind(permissionsController));
router.get("/permissions",permissionsController.list.bind(permissionsController));
router.get("/permissions/:id",permissionsController.get.bind(permissionsController));
router.put("/permissions/:id",permissionsController.update.bind(permissionsController));
router.delete("/permissions/:id",permissionsController.remove.bind(permissionsController));

export default router;
