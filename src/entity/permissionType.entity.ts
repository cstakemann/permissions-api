import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  OneToOne
} from "typeorm";
import { Permission } from "./permission.entity";

@Entity()
export class PermissionType {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  title!: string;

  @OneToOne(() => Permission, (permission) => permission.permissionType)
  permission!: Permission;

  @CreateDateColumn()
  createdAt!: Date;

  @UpdateDateColumn()
  updatedAt!: Date;
}
