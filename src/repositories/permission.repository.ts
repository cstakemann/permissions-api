import { NotFound } from "http-errors";
import { DatabaseRepository, Id, Query } from "../declarations/declaration";
import database from "../config/database.config";
import { Permission } from "../entity/permission.entity";

export class PermissionRepository implements DatabaseRepository<Permission> {

  async create(data: Partial<Permission>, query?: Query | undefined): Promise<Permission> {
    const repository = database.getRepository(Permission);
    const permission = repository.create(data);
    await repository.save(permission);
    return permission;
  }
  
  async list(query?: Query | undefined): Promise<Permission[]> {
    const repository = database.getRepository(Permission);
    return await repository.find();
  }

  async get(id: Id, query?: Query | undefined): Promise<Permission> {
    const repository = database.getRepository(Permission);
    const permission = await repository.findOneBy({id: id as any});
    
    if(!permission) throw new NotFound("Permission does not exist")

    return permission;
  }

  async update(id: Id, data: Permission, query?: Query | undefined): Promise<Permission> {
    const repository = database.getRepository(Permission);
    await repository.update(id, data);

    return await this.get(id,query);
  }

  async remove(id: Id, query?: Query | undefined): Promise<Permission> {
    const permission = await this.get(id,query);
    const repository = database.getRepository(Permission);

    await repository.delete(id);

    return permission;
  }
}
