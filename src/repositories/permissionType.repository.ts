import { DatabaseRepository, Id, Query } from "../declarations/declaration";
import database from "../config/database.config";
import { PermissionType } from "../entity/permissionType.entity";
import { NotFound } from "http-errors";

export class PermissionTypeRepository implements DatabaseRepository<PermissionType> {

  async create(data: Partial<PermissionType>, query?: Query | undefined): Promise<PermissionType> {
    const repository = database.getRepository(PermissionType);
    const permissionType = repository.create(data);
    await repository.save(permissionType);
    return permissionType;
  }

  async list(query?: Query | undefined): Promise<PermissionType[]> {
    const repository = database.getRepository(PermissionType);
    return await repository.find();
  }

  async get(id: Id, query?: Query | undefined): Promise<PermissionType> {
    const repository = database.getRepository(PermissionType);
    const permissionType = await repository.findOneBy({id: id as any});
    
    if(!permissionType) throw new NotFound("Permission type does not exist")

    return permissionType;
  }

  async update(id: Id, data: PermissionType, query?: Query | undefined): Promise<PermissionType> {
    const repository = database.getRepository(PermissionType);
    await repository.update(id, data);

    return await this.get(id,query);
  }

  async remove(id: Id, query?: Query | undefined): Promise<PermissionType> {
    const permissionType = await this.get(id,query);
    const repository = database.getRepository(PermissionType);

    await repository.delete(id);

    return permissionType;
  }

}
