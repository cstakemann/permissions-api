import { DataSource } from "typeorm";
import { Permission } from "../entity/permission.entity";
import { PermissionType } from "../entity/permissionType.entity";

export default new DataSource({
  type: "mysql",
  host: "localhost",
  port: 3306,
  username: "root",
  password: "root",
  database: "permissions_db",
  entities: [Permission, PermissionType],
  synchronize: true,
  logging: false,
});
