import { Request, Response, NextFunction } from "express";
import { DatabaseRepository } from "../declarations/declaration";
import { Permission } from "../entity/permission.entity";
import { PermissionType } from "../entity/permissionType.entity";
const Joi = require('joi');

const createValidator = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
  permissionType: Joi.number().options({ convert: false }).required(),
})

const updateValidator = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
  permissionType: Joi.number().options({ convert: false }),
})

export class PermissionController {
  constructor(private permissionRepository: DatabaseRepository<Permission>, private permissionTypeRepository: DatabaseRepository<PermissionType>) {}

  async create(req: Request, res: Response, next: NextFunction): Promise<any> {
    try {
      const body = req.body;
      const { error } = await createValidator.validate(body);
      if(error) {
        return res.status(400).json(error.details);
      }

      const permissionType = await this.permissionTypeRepository.get(body.permissionType);
      body.permissionType = permissionType;
      const permission = await this.permissionRepository.create(body);
      return res.status(200).json(permission);
    } catch (error) {
      next(error)
    }
  }

  async list(req: Request, res: Response, next: NextFunction): Promise<any> {
    try {
      const permissions = await this.permissionRepository.list();
      return res.status(200).json(permissions);
    } catch (error) {
      next(error)
    }
  }

  async get(req: Request, res: Response, next: NextFunction): Promise<any> {
    try {
      const { id } = req.params;
      const permission = await this.permissionRepository.get(id);
      return res.status(200).json(permission);
    } catch (error) {
      next(error)
    }
  }

  async update(req: Request, res: Response, next: NextFunction): Promise<any> {
    try {
      const { id } = req.params;
      const body = req.body;

      const { error } = await updateValidator.validate(body);
      if(error) {
        return res.status(400).json(error.details);
      }

      const permissionType = await this.permissionTypeRepository.get(body.permissionType);
      body.permissionType = permissionType;

      const permission = await this.permissionRepository.update(id, body);
      return res.status(200).json(permission);
    } catch (error) {
      next(error)
    }
  }

  async remove(req: Request, res: Response, next: NextFunction): Promise<any> {
    try {
      const { id } = req.params;
      const permission = await this.permissionRepository.remove(id);
      return res.status(200).json(permission);
    } catch (error) {
      next(error)
    }
  }
}
