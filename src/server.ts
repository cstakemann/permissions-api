import express, { Application } from "express";
import dotenv from "dotenv";
import morgan from "morgan";
import "reflect-metadata";
import database from "./config/database.config";
import PermissionRoutes from "./routes/permission.routes";

export class Server {
  public app: Application;

  constructor() {
    this.app = express();
    this.settings();
    this.middlewares();
    this.routes();
  }

  settings() {
    dotenv.config();
    database
      .initialize()
      .then(() => console.log("Database connected"))
      .catch(console.error);
  }

  middlewares() {
    this.app.use(morgan("dev"));
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
  }
  routes() {
    this.app.use("/api", PermissionRoutes);
  }

  async listen() {
    await this.app.listen(process.env.PORT);
    console.log("Server on port", process.env.PORT);
  }
}
