# permissions api

## How to run?

- [ ] Clone this repo
- [ ] Create mysql database `permissions_db`
- [ ] Set mysql credentials into `src/config/database.config.ts`
- [ ] Run `npm i` in your terminal
- [ ] Run `npm run dev` in your terminal
- [ ] Import data into table permission_type(this data is in the file `permission_type.sql`)

## Notes:
* When you run `npm run dev` the tables `permission` and `permission_type` are created

```
cd existing_repo
git remote add origin https://gitlab.com/cstakemann/permissions-api.git
git branch -M main
git push -uf origin main
```

## Test and Deploy

- [ ] execute `npm run dev` in your terminal to serve
- [ ] execute `npm run prod` in your terminal to build and start when is in production


## Use

Open postman and make a request to the following endpoints:

* Create permission
```
URl: localhost:3000/api/permissions/
Method: post
Body raw: 
{
    "title": "Example title",
    "description": "example",
    "permissionType": 1
}
```

* list permissions
```
URl: localhost:3000/api/permissions/
Method: get
```

* get permission
```
URl: localhost:3000/api/permissions/:id
Method: get
```

* update permission
```
URl: localhost:3000/api/permissions/:id
Method: put
Body raw: 
{
    "title": "Example title",
    "description": "example",
    "permissionType": 1
}
```

* remove permission
```
URl: localhost:3000/api/permissions/:id
Method: delete
Body raw: 
{
    "title": "Example title",
    "description": "example",
    "permissionType": 1
}
```
